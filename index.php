<?php

require_once 'vendor/autoload.php';

$inputFileName = dirname(__FILE__).'/file/Type_A.xlsx';
$validate   = new \ValidateExcelData\Check();
$resp       = $validate->checkExcel($inputFileName);
$result     = json_encode($resp);
echo $result;

