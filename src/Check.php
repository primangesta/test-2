<?php

namespace ValidateExcelData;

class Check
{
    public function checkExcel($ExcelFile)
    {
        $inputFileType  = \PhpOffice\PhpSpreadsheet\IOFactory::identify($ExcelFile);
        $reader         = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet    = $reader->load($ExcelFile);
        $worksheet      = $spreadsheet->getActiveSheet();

        $lastColumn = $worksheet->getHighestColumn();
        $lastRow    = $worksheet->getHighestRow();
        $lastColumn++;
        $lastRow++;
        $rows = [];
        $object = new \stdClass();
        for($column = 'A'; $column != $lastColumn; $column++) {
            for($row = 1; $row != $lastRow; $row++) {
                $cell = $worksheet->getCell($column.$row)->getValue();
                if($row == 1){
                    $nameHeader     = str_replace(array('*','#'), '',$cell);;
                    $startCondition = substr($cell, 0, 1);
                    $endCondition   = substr($cell, -1);
                }else{
                    if($startCondition == '#'){
                        if($cell == trim($cell) && strpos($cell, ' ') !== false) {
                            if (array_key_exists($row, $rows)) {
                                $rows[$row] = $rows[$row].', '.$nameHeader.' should not contain any space';
                            }else{
                                $rows[$row] = $nameHeader.' should not contain any space';
                            }
                        }
                    }

                    if($endCondition == '*'){
                        if($cell == null){
                            if (array_key_exists($row, $rows)) {
                                $rows[$row] = $rows[$row].', '.'Missing value in '.$nameHeader;
                            }else{
                                $rows[$row] = 'Missing value in '.$nameHeader;
                            }
                        }
                    }
                }
            }
        }
        
        $result = [];
        foreach($rows as $key => $value){
            $temp = [];
            $temp['row']    = $key;
            $temp['error']  = $value;
            array_push($result, $temp);
        }

        return $result;
    }
}
